<!DOCTYPE html>
<html>

<head>
    <title> Tabel Perkalian </title>
</head>

<body>
    <div align="center">
        <h1> Tabel Perkalian 1x1 sampai 5x5</h1>
        <form name="frm">
            masukkan angka:
            <input type="number" name="n">
            <br><br>
            <input type="submit" name="sub">

        </form>
        <table border="2">
            <tr>
                <th>X</th>
                <?php
                for ($n = 1; $n <= 5; $n++) {
                    echo '<th>' . $n . '</th>';
                }
                ?>
            </tr>
            <?php
            for ($o = 1; $o <= 5; $o++) {
                echo '<tr>
                        <th>' . $o . '</th>';
                for ($p = 1; $p <= 5; $p++) {
                    $product = $o * $p;
                    if ($o == $p) {
                        echo "<td style='background-color: #FA8072; color; #000000;'>" . $product . "</td>";
                    } else {
                        echo "<td>" . $product . "</td>";
                    }
                }
            }
            ?>
        </table>
    </div>
</body>

</html>